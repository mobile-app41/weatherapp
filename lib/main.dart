import 'package:flutter/material.dart';

void main() {
  runApp(WeatherApp());
}
class WeatherApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: buildBodyWidget(),
      ),
    );
  }

  Widget buildBodyWidget(){
    return ListView (
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: NetworkImage("https://p0.piqsels.com/preview/631/424/841/air-atmosphere-background-beautiful.jpg"),
              fit: BoxFit.cover,
            ),
        ),

        child: Column(
              children: <Widget>[
                Text('แพร่',
                  style: TextStyle(
                      fontSize: 35,color: Colors.white
                  ),),
                Text('19°',
                  style: TextStyle(
                      fontSize: 100,color: Colors.white
                  ),),
                Text('เมฆเป็นส่วนมาก',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,color: Colors.white
                  ),),
                Text('สูงสุด: 25° ต่ำสุด: 19°',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,color: Colors.white
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.all(0),
                  height: 150,
                  width:double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.blue.shade900,
                    borderRadius: BorderRadius.circular(20), //border corner radius
                    boxShadow:[
                      BoxShadow(
                        color: Colors.white.withOpacity(0), //color of shadow
                        spreadRadius: 5, //spread radius
                        blurRadius: 7, // blur radius
                        offset: Offset(0, 2), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Column(
                      children: <Widget>[
                        Text('คาดว่าฝนจะตกเป็นเวลา 16.00',
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                            height:2.3,
                          ),
                        ),
                        Divider(
                          indent: 20,
                          thickness: 0,
                          color: Colors.white,
                        )
                        ,timeDay(),
                        itemsIcon(),
                      ]
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  margin: EdgeInsets.all(0),
                  height:350,
                  width:double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.blue.shade900,
                    borderRadius: BorderRadius.circular(20), //border corner radius
                    boxShadow:[
                      BoxShadow(
                        color: Colors.white.withOpacity(0), //color of shadow
                        spreadRadius: 5, //spread radius
                        blurRadius: 7, // blur radius
                        offset: Offset(0, 2), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Column(
                      children: <Widget>[
                        buildCalendar(),
                        Divider(
                          indent: 20,
                          thickness: 0,
                          color: Colors.white,
                        ),
                        buildToday(),
                        buildWednesday(),
                        buildThursday(),
                        buildFriday(),
                        buildSaturday(),
                        buildSunday(),
                        buildMonday(),
                      ]
                  ),
                ),
              ],
          ),
        ),
      ],
    );
  }
}
Widget timeDay(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('6',style: TextStyle(
            color: Colors.white)),
        Text('7',style: TextStyle(
            color: Colors.white)),
        Text('8',style: TextStyle(
            color: Colors.white)),
        Text('9',style: TextStyle(
            color: Colors.white)),
        Text('10',style: TextStyle(
            color: Colors.white)),
        Text('11',style: TextStyle(
            color: Colors.white)),
      ]
  );
}
Widget buildCloudySnow() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
          color: Colors.white,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('19°',style: TextStyle(
          color: Colors.white)),
    ],
  );
}
Widget buildCloudy() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('25°',style: TextStyle(
          color: Colors.white)),
    ],
  );
}
Widget buildWbSunny() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.wb_sunny_outlined,
          color: Colors.white,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('29°',style: TextStyle(
          color: Colors.white)),
    ],
  );
}
Widget buildSunny() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.amber,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('29°',style: TextStyle(
          color: Colors.white)),
    ],
  );
}

Widget itemsIcon(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        buildSunny(),
        buildCloudy(),
        buildWbSunny(),
        buildWbSunny(),
        buildCloudy(),
        buildCloudySnow(),
      ]
  );
}
Widget buildCalendar() {
  return Row(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.calendar_today,
          color: Colors.white,
          size: 18,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('พยากรณ์อากาศ 7 วัน',
        style: TextStyle(
          fontSize: 16,
          color: Colors.white,
          height:0,
          fontWeight: FontWeight.bold,
        ),
      )
    ],
  );
}

Widget buildToday(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('อ.   ',style: TextStyle(
            color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('19°',style: TextStyle(
            color: Colors.white)),
        Text('🌡️',style: TextStyle(
            color: Colors.deepOrange)),
        Text('25°',style: TextStyle(
            color: Colors.white)),
      ]
  );
}
Widget buildWednesday(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('พ.   ',style: TextStyle(
            color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('21°',style: TextStyle(
            color: Colors.white)),
        Text('🌡️',style: TextStyle(
            color: Colors.deepOrange)),
        Text('32°',style: TextStyle(
            color: Colors.white)),
      ]
  );
}

Widget buildThursday(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('พฤ.',style: TextStyle(
            color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('19°',style: TextStyle(
            color: Colors.white)),
        Text('🌡️',style: TextStyle(
            color: Colors.deepOrange)),
        Text('25°',style: TextStyle(
            color: Colors.white)),
      ]
  );
}
Widget buildFriday() {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('ศ.   ', style: TextStyle(
            color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('21°', style: TextStyle(
            color: Colors.white)),
        Text('🌡️', style: TextStyle(
            color: Colors.deepOrange)),
        Text('32°', style: TextStyle(
            color: Colors.white)),
      ]
  );
}
Widget buildSaturday(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('ส.   ',style: TextStyle(
            color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('21°',style: TextStyle(
            color: Colors.white)),
        Text('🌡️',style: TextStyle(
            color: Colors.deepOrange)),
        Text('32°',style: TextStyle(
            color: Colors.white)),
      ]
  );
}

Widget buildSunday(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('อา.   ',style: TextStyle(
            color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('21°',style: TextStyle(
            color: Colors.white)),
        Text('🌡️',style: TextStyle(
            color: Colors.deepOrange)),
        Text('32°',style: TextStyle(
            color: Colors.white)),
      ]
  );
}
Widget buildMonday(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('จ.   ',style: TextStyle(
            color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('21°',style: TextStyle(
            color: Colors.white)),
        Text('🌡️',style: TextStyle(
            color: Colors.deepOrange)),
        Text('31°',style: TextStyle(
            color: Colors.white)),
      ]
  );
}



